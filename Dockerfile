FROM evoluumbr/evoluum-ci-t2b:1.1

RUN printf '%s\n' \
    '#!/bin/bash' \
    'mvn -B verify' \
    'echo oi1=$VAR01' \
    'echo oi2=$VAR02' \    
    'echo BITBUCKET_BRANCH=$BITBUCKET_BRANCH' \    
    'echo BITBUCKET_COMMIT=$BITBUCKET_COMMIT' \    
    > script.sh

ENV HOMOLOG_BUILD=/script.sh

